Tiny API for DiGEiZ for a technical test
========================================

**based on [Python Boilerplate](https://github.com/digeiz-sas/python_boilerplate) from DiGEiZ**


Requirement
-----------

* Python 3.8
* virtualenv

If using an apple arm64
-----------------------

#### You will need:

* homebrew

**in `setup.py` change `psycopg2-binary==2.8.5` by `psycopg2`**

And then, execute

```shell
sh build_psycopg2_macos_arm64.sh && pip install psycopg2 --no-index -f ./dist/psycopg2-2.8.5
```

Usage
-----

### Set up a virtualenv (recommanded):

```shell
{python_bin} -m virtualenv {your_venv_dir}
source /{your_venv_dir}/bin/activate
```

#### Install the application:

```shell
pip install -e ".[dev]"
```

#### Run the application:

```shell
digeiz-api-ws
```

#### Update:

```shell
pip install -e ".[dev]" -U --upgrade-strategy eager
```

#### After developing, the full test suite can be evaluated by running:

```shell
pytest . --cov=digeiz # Use -v -s for verbose mode
```

TODO
----

* Handle config management
* Improve test coverage (Actual = 82%)
* Improve unit testing by reducing `mall` and `unit` dependencies to their parents
* Improve error handling by creating dedicated Exceptions
* Fix parameters given in request when they don't correspond to their type or when they're missing
* Finish `connector.py` for a future interactions with a PostgreSQL DB and remove sqlite DB

Endpoints
---------

### Accounts:

* GET `http://{HOST}:{PORT}/api/v1/accounts{?next=<int:next> || ""}`
* POST `http://{HOST}:{PORT}/api/v1/accounts`
* GET `http://{HOST}:{PORT}/api/v1/accounts/<int:account_uid>`
* DELETE `http://{HOST}:{PORT}/api/v1/accounts/<int:account_uid>`
* PUT `http://{HOST}:{PORT}/api/v1/accounts/<int:account_uid>`

### Malls:

* GET `http://{HOST}:{PORT}/api/v1/malls{?next=<int:next> || ""}`
* POST `http://{HOST}:{PORT}/api/v1/malls`
* GET `http://{HOST}:{PORT}/api/v1/malls/<int:mall_uid>`
* DELETE `http://{HOST}:{PORT}/api/v1/malls/<int:mall_uid>`
* PUT `http://{HOST}:{PORT}/api/v1/malls/<int:mall_uid>`

### Units:

* GET `http://{HOST}:{PORT}/api/v1/units{?next=<int:next> || ""}`
* POST `http://{HOST}:{PORT}/api/v1/units`
* GET `http://{HOST}:{PORT}/api/v1/units/<int:unit_uid>`
* DELETE `http://{HOST}:{PORT}/api/v1/units/<int:unit_uid>`
* PUT `http://{HOST}:{PORT}/api/v1/units/<int:unit_uid>`