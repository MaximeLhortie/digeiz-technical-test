CREATE TABLE IF NOT EXISTS account (
    uid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS mall (
    uid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT NOT NULL,
    account_uid INTEGER NOT NULL,
    FOREIGN KEY (account_uid) REFERENCES account(uid)
);
CREATE TABLE IF NOT EXISTS unit (
    uid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT NOT NULL,
    mall_uid INTEGER NOT NULL,
    FOREIGN KEY (mall_uid) REFERENCES mall(uid)
);
INSERT INTO account (name)
VALUES ("westfield"),
    ("unibail"),
    ("jouy-en-josas"),
    ("dupont"),
    ("toto");
INSERT INTO mall (name, account_uid)
VALUES ("velizy2", 1),
    ("sowest", 1),
    ("belle epine", 2),
    ("foire à la saucisse", 3),
    ("agora", 4),
    ("carre senart", 5);
INSERT INTO unit (name, mall_uid)
VALUES ("tesla", 1),
    ("apple", 1),
    ("marks&spencer", 2),
    ("darty", 3),
    ("fnac", 3),
    ("boucherie chez gégé", 4),
    ("footlocker", 5),
    ("disney store", 5);