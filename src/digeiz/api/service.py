import os

from flask import Flask

from digeiz.api.utils.extensions import api, db, register_extensions
from digeiz.api.endpoints import (
    AccountListResource,
    AccountResource,
    MallListResource,
    MallResource,
    UnitListResource,
    UnitResource,
)


def create_app(config=None):
    root_app = Flask(__name__)
    if config is not None:
        root_app.config.from_mapping(config)
    else:
        basedir = os.path.abspath(os.path.dirname(__file__))
        root_app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{os.path.join(basedir, 'api.db')}"
        root_app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

    root_app.debug = True
    return root_app


API_PREFIX = "/api/v1"

# Accounts endpoints mounting
api.add_resource(AccountListResource, f"{API_PREFIX}/accounts")
api.add_resource(AccountResource, f"{API_PREFIX}/accounts/<int:account_uid>")

# Malls endpoints mounting
api.add_resource(MallListResource, f"{API_PREFIX}/malls")
api.add_resource(MallResource, f"{API_PREFIX}/malls/<int:mall_uid>")

# Units endpoints mounting
api.add_resource(UnitListResource, f"{API_PREFIX}/units")
api.add_resource(UnitResource, f"{API_PREFIX}/units/<int:unit_uid>")


app = create_app()


def run():
    register_extensions(app)
    with app.app_context():
        db.create_all()
    app.run(debug=True)


if __name__ == "__main__":
    run()
