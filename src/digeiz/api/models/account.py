from digeiz.api.utils.extensions import db


class Account(db.Model):
    uid = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    name = db.Column(db.String(150), nullable=False)

    def __init__(self, uid: int, name: str):
        self.uid = uid
        self.name = name

    @classmethod
    def find_by_name(cls, name: str):
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_by_id(cls, uid: int):
        return cls.query.filter_by(uid=uid).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.commit()

    def update_in_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
