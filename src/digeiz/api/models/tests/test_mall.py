from digeiz.api.models import Mall


def test_create_account_obj():
    account = Mall(1, "mall_test", 1)

    assert account.uid == 1
    assert account.name == "mall_test"


def test_save_account_obj(app):
    account = Mall(1, "mall_test", 1)
    account.add_to_db()

    account = Mall.find_by_id(uid=1)
    assert account.uid == 1
    assert account.name == "mall_test"


def test_update_account_obj(app):
    account = Mall(1, "mall_test", 1)
    account.add_to_db()

    account.name = "mall_test_name_updated"
    account.update_in_db()

    account = Mall.find_by_id(uid=1)
    assert account.uid == 1
    assert account.name == "mall_test_name_updated"


def test_delete_account_obj(app):
    account = Mall(1, "mall_test", 1)
    account.add_to_db()

    account = Mall.find_by_id(uid=1)
    assert account
    assert account.uid == 1

    account.delete_from_db()
    account = Mall.find_by_id(uid=1)
    assert account is None
