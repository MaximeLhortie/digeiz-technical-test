from digeiz.api.models import Account


def test_create_account_obj():
    account = Account(1, "account_test")

    assert account.uid == 1
    assert account.name == "account_test"


def test_save_account_obj(app):
    account = Account(1, "account_test")
    account.add_to_db()

    account = Account.find_by_id(uid=1)
    assert account.uid == 1
    assert account.name == "account_test"


def test_update_account_obj(app):
    account = Account(1, "account_test")
    account.add_to_db()

    account.name = "account_test_name_updated"
    account.update_in_db()

    account = Account.find_by_id(uid=1)
    assert account.uid == 1
    assert account.name == "account_test_name_updated"


def test_delete_account_obj(app):
    account = Account(1, "account_test")
    account.add_to_db()

    account = Account.find_by_id(uid=1)
    assert account
    assert account.uid == 1

    account.delete_from_db()
    account = Account.find_by_id(uid=1)
    assert account is None
