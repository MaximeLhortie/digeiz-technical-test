import os
import json
import pytest

from digeiz.api import service


# def pytest_configure(config):
# os.environ["DIGEIZ_SETTINGS_MODULE"] = "digeiz.api.settings.unittests"


@pytest.fixture
def app():
    """Fixture that provide app and context to tests who need it"""

    basedir = os.path.abspath(os.path.dirname(__file__))

    service.app.config["TESTING"] = True
    service.app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = f"sqlite:///{os.path.join(basedir, 'test_api.db')}"

    if "sqlalchemy" not in service.app.extensions:
        service.register_extensions(app=service.app)

    with service.app.app_context():
        service.db.create_all()
        yield service.app
        service.db.drop_all()


def _external_fixture_path(request, name=None, prefix=None, suffix=None):
    """ "
    See https://docs.pytest.org/en/latest/builtin.html#fixtures-and-requests

    @pytest.mark.external_fixture(name='FixtureName', suffix='.toto')
    def test_content(external_fixture_path):
        ...
    """
    marker = request.node.get_closest_marker("external_fixture")
    marker_args = marker and marker.kwargs or {}

    if suffix is None:
        suffix = marker_args.get("suffix") or ""

    if prefix is None:
        prefix = marker_args.get("prefix")

    if prefix is None:
        base_path = os.path.splitext(request.module.__file__)[0]
    else:
        base_path = os.path.join(os.path.dirname(os.path.realpath(request.module.__file__)), prefix)

    if name is None:
        name = marker_args.get("name") or (request.function.__name__.replace("test_", ""))

    return base_path + "-" + name + suffix


@pytest.fixture
def load_fixture_file(request):
    def _nested(filename):
        path = os.path.join(os.path.dirname(os.path.realpath(request.module.__file__)), filename)
        with open(path) as fixture:
            return fixture.read()

    return _nested


@pytest.fixture
def load_external_fixture(request):
    def _nested(**kwords):
        path = _external_fixture_path(request, **kwords)
        with open(path) as fixture:
            return fixture.read()

    return _nested


@pytest.fixture
def external_fixture_path(request):
    return _external_fixture_path(request)


@pytest.fixture
def xml_fixture_path(request):
    return _external_fixture_path(request, suffix=".xml")


@pytest.fixture
def yaml_fixture_path(request):
    return _external_fixture_path(request, suffix=".yml")


@pytest.fixture
def json_fixture_path(request):
    return _external_fixture_path(request, suffix=".json")


@pytest.fixture
def xml_fixture(xml_fixture_path):
    with open(xml_fixture_path) as fixture:
        yield fixture.read()


@pytest.fixture
def json_fixture(json_fixture_path):
    with open(json_fixture_path) as fixture:
        yield fixture.read()


@pytest.fixture
def parsed_json_fixture(json_fixture_path):
    with open(json_fixture_path) as fixture:
        yield json.load(fixture)
