from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api

db = SQLAlchemy()
ma = Marshmallow()
api = Api()


def register_extensions(app):
    db.init_app(app)
    ma.init_app(app)
    api.init_app(app)
