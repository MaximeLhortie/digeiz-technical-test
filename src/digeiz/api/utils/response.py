from flask import make_response, jsonify


def abort_service_error():
    return make_response({"msg": "Service error"}, 500)


def abort_not_found(collection: str, uid: int):
    return make_response({"msg": f"{collection} with uid {uid} not found"}, 404)


def abort_invalid_request(msg: str):
    return make_response({"msg": msg}, 400)
