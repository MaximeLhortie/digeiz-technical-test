from .utils.version import get_version

__VERSION_TUPLE__ = (0, 4, 0, "alpha", 1)
__VERSION__ = get_version(__VERSION_TUPLE__)

__all__ = ["__VERSION__", "__VERSION_TUPLE__"]
