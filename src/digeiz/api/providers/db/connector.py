from typing import Optional

import psycopg2

#
# WARNING
#
# WIP: do not use


class DBCoonector:
    """DataBase connector (PostgreSQL)"""

    db_name: str
    db_user: str
    db_passwd: str
    db_host: str
    db_port: int
    _db_conn = None  # psycopg2 connection to DB
    _db_cursor = None  # psycopg2 DB cursor

    def __init__(
        self,
        db_host: str,
        db_name: str,
        db_user: str,
        db_port: Optional[int] = None,
        db_passwd: Optional[str] = None,
    ):
        self.db_name = db_name
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_host = db_host
        self.db_port = db_port

    def __enter__(self):
        self._db_conn = psycopg2.connect(host=self.db_host, dbname=self.db_name, user=self.db_user)
        self._db_cursor = self._db_conn.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._db_cursor.close()
        self._db_conn.close()
        return self

    def _execute(self, sql_req: str):
        if self._db_conn and self._db_cursor:
            self._db_cursor.execute(sql_req)

    def _commit(self):
        if self._db_conn and self._db_cursor:
            self._db_conn.commit()

    def prepare_and_commit(self, sql_req: str):
        if sql_req:
            self._execute(sql_req=sql_req)
            self._commit()
