from digeiz.api.utils.extensions import ma
from digeiz.api.models import Mall


class MallSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Mall
        include_fk = True


mall_schema = MallSchema()
malls_schema = MallSchema(many=True)
