from digeiz.api.utils.extensions import ma
from digeiz.api.models import Account


class AccountSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Account


account_schema = AccountSchema()
accounts_schema = AccountSchema(many=True)
