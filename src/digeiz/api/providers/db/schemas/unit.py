from digeiz.api.utils.extensions import ma
from digeiz.api.models import Unit


class UnitSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Unit
        include_fk = True


unit_schema = UnitSchema()
units_schema = UnitSchema(many=True)
