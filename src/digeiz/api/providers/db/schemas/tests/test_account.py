from digeiz.api.models import Account
from digeiz.api.providers.db.schemas import account_schema, accounts_schema


def test_dump_account(app):
    assert account_schema.Meta.model == Account

    result = account_schema.dump(Account(1, "account_test"))

    assert result == {"name": "account_test", "uid": 1}


def test_dump_accounts(app):
    assert account_schema.Meta.model == Account

    result = accounts_schema.dump([Account(1, "account_test"), Account(2, "another_account_test")])

    assert result == [
        {"name": "account_test", "uid": 1},
        {"name": "another_account_test", "uid": 2},
    ]
