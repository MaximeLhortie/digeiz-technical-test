from digeiz.api.models import Mall
from digeiz.api.providers.db.schemas import mall_schema, malls_schema


def test_dump_account(app):
    assert mall_schema.Meta.model == Mall

    result = mall_schema.dump(Mall(1, "mall_test", 1))

    assert result == {"account_uid": 1, "name": "mall_test", "uid": 1}


def test_dump_accounts(app):
    assert mall_schema.Meta.model == Mall

    result = malls_schema.dump([Mall(1, "mall_test", 1), Mall(2, "another_mall_test", 1)])

    assert result == [
        {"account_uid": 1, "name": "mall_test", "uid": 1},
        {"account_uid": 1, "name": "another_mall_test", "uid": 2},
    ]
