from digeiz.api.models import Unit
from digeiz.api.providers.db.schemas import unit_schema, units_schema


def test_dump_account(app):
    assert unit_schema.Meta.model == Unit

    result = unit_schema.dump(Unit(1, "unit_test", 1))

    assert result == {"mall_uid": 1, "name": "unit_test", "uid": 1}


def test_dump_accounts(app):
    assert unit_schema.Meta.model == Unit

    result = units_schema.dump([Unit(1, "unit_test", 1), Unit(2, "another_unit_test", 1)])

    assert result == [
        {"mall_uid": 1, "name": "unit_test", "uid": 1},
        {"mall_uid": 1, "name": "another_unit_test", "uid": 2},
    ]
