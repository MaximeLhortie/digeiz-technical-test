from flask import jsonify, make_response
from flask_restful import Resource, reqparse

from digeiz.api.models import Unit, Mall
from digeiz.api.providers.db.schemas import unit_schema, units_schema
from digeiz.api.utils.response import abort_invalid_request, abort_not_found, abort_service_error

form_parser = reqparse.RequestParser()
form_parser.add_argument("uid", type=int)
form_parser.add_argument("name", type=str, required=True, help="'name' has to be provided")
form_parser.add_argument(
    "mall_uid",
    type=int,
    required=True,
    help="'mall_uid' has to be provided to link new/given unit/shop to a mall",
)

query_parser = reqparse.RequestParser()
query_parser.add_argument("next", type=int, default=1)


class UnitListResource(Resource):
    def get(self):
        try:
            next_uid = query_parser.parse_args().get("next") if not 0 else 1
            if next_uid < 0:
                abort_invalid_request(msg="?next params can't be negative")

            all_units = Unit.query.all()
            units = all_units[next_uid - 1 :]

            if len(units) > 2:
                next_uid = units[next_uid - 1 : 3][-1].uid
                units = units[:2]
            else:
                next_uid = None

            return make_response(
                jsonify(
                    {
                        "data": units_schema.dump(units),
                        "count": len(units),
                        "next": next_uid,
                    }
                ),
                200,
            )
        except Exception as e:
            return abort_service_error()

    def post(self):
        try:
            args = form_parser.parse_args()
            owner_mall = Mall.find_by_id(uid=args.get("mall_uid"))

            if not owner_mall:
                return abort_invalid_request(msg="There is no mall for the given account_uid")

            if args.get("uid") and Unit.find_by_id(uid=args.get("uid")):
                return abort_invalid_request(msg="A unit/shop already exist with the given uid")

            unit = Unit(
                uid=args.get("uid", None), name=args.get("name"), mall_uid=args.get("mall_uid")
            )
            unit.add_to_db()
            return make_response(
                {
                    "msg": "Mall has been created and added in DB",
                    "data": {"uid": unit.uid, "name": unit.name, "mall_uid": unit.mall_uid},
                },
                201,
            )
        except Exception as e:
            return abort_service_error()


class UnitResource(Resource):
    def get(self, unit_uid: int):
        try:
            account = Unit.query.get(unit_uid)
            if account is None:
                return abort_not_found(collection="unit", uid=unit_uid)
            else:
                return make_response(jsonify({"data": unit_schema.jsonify(account).json}), 200)
        except Exception as e:
            return abort_service_error()

    def delete(self, unit_uid: int):
        try:
            unit = Unit.find_by_id(uid=unit_uid)
            if not unit:
                return abort_invalid_request(msg="unit with the given uid doesn't exist")

            unit.delete_from_db()
            return make_response({"msg": "Unit has been removed from DB"}, 200)
        except Exception as e:
            return abort_service_error()

    def put(self, unit_uid: int):
        try:
            unit = Unit.find_by_id(uid=unit_uid)
            args = form_parser.parse_args()
            if not unit:
                return abort_invalid_request(msg="There is no unit for the given unit_uid")

            if args.get("uid"):
                return abort_invalid_request(msg="you can't update an unit uid")

            unit.name = args.get("name", unit.name)
            unit.mall_uid = args.get("mall_uid", unit.mall_uid)

            unit.update_in_db()
            return make_response(
                {
                    "msg": "Unit has been updated in DB",
                    "data": {"uid": unit.uid, "name": unit.name, "mall_uid": unit.mall_uid},
                },
                200,
            )
        except Exception as e:
            return abort_service_error()
