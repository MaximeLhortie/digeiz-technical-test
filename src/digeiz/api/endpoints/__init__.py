from .accounts import AccountResource, AccountListResource
from .malls import MallResource, MallListResource
from .units import UnitResource, UnitListResource
