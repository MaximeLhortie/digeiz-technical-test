from flask import jsonify, make_response
from flask_restful import Resource, reqparse

from digeiz.api.models import Account
from digeiz.api.providers.db.schemas import account_schema, accounts_schema
from digeiz.api.utils.response import abort_service_error, abort_not_found, abort_invalid_request

form_parser = reqparse.RequestParser()
form_parser.add_argument("uid", type=int)
form_parser.add_argument("name", type=str, help="'name‘ has to be provided")

query_parser = reqparse.RequestParser()
query_parser.add_argument("next", type=int, default=1)


class AccountListResource(Resource):
    def get(self):
        try:
            next_uid = query_parser.parse_args().get("next") if not 0 else 1
            if next_uid < 0:
                return abort_invalid_request(msg="?next params can't be negative")

            all_accounts = Account.query.all()
            accounts = all_accounts[next_uid - 1 :]

            if len(accounts) > 2:
                next_uid = accounts[next_uid - 1 : 3][-1].uid
                accounts = accounts[:2]
            else:
                next_uid = None

            return make_response(
                jsonify(
                    {
                        "data": accounts_schema.dump(accounts),
                        "count": len(accounts),
                        "next": next_uid,
                    }
                ),
                200,
            )
        except Exception as e:
            return abort_service_error()

    def post(self):
        try:
            args = form_parser.parse_args()
            if args.get("uid") and Account.find_by_id(uid=args.get("uid")):
                return abort_not_found(collection="account", uid=args.get("uid"))

            account = Account(
                args.get("uid", None), args.get("name")
            )  # UID can be NONE because column has autoincrement rule
            account.add_to_db()

            return make_response(
                {
                    "msg": "Account has been created and added in DB",
                    "data": {"uid": account.uid, "name": account.name},
                },
                201,
            )
        except Exception as e:
            return abort_service_error()


class AccountResource(Resource):
    def get(self, account_uid: int):
        try:
            account = Account.query.get(account_uid)
            if account is None:
                return abort_not_found(collection="account", uid=account_uid)
            else:
                return make_response(jsonify({"data": account_schema.jsonify(account).json}), 200)
        except Exception as e:
            return abort_service_error()

    def delete(self, account_uid: int):
        try:
            account = Account.find_by_id(uid=account_uid)

            if not account:
                return abort_not_found(collection="account", uid=account_uid)

            account.delete_from_db()

            return make_response({"msg": "Account has been removed from DB"}, 200)
        except Exception as e:
            return abort_service_error()

    def put(self, account_uid: int):
        try:
            account = Account.find_by_id(uid=account_uid)
            args = form_parser.parse_args()

            if not account:
                return abort_not_found(collection="account", uid=account_uid)

            if args.get("uid"):
                return abort_invalid_request("you can't update an account uid")

            account.name = args.get("name", account.name)
            account.update_in_db()

            return make_response(
                {
                    "msg": "Account has been updated in DB",
                    "data": {"uid": account.uid, "name": account.name},
                },
                200,
            )
        except Exception as e:
            return abort_service_error()
