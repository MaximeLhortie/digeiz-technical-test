def test_post_mall_no_account(app):
    payload = {"name": "insert_test", "account_uid": 1}

    with app.test_client() as client:
        result = client.post("/api/v1/malls", json=payload)

    assert result.status_code == 400
    assert result.json
    assert result.json.get("msg")


def test_post_mall(app):
    payload = {"name": "insert_test", "account_uid": 1}

    with app.test_client() as client:
        client.post("/api/v1/accounts", json={"name": "account_test"})
        result = client.post("/api/v1/malls", json=payload)

    assert result.status_code == 201
    assert result.json
    assert result.json.get("data", {}).get("name") == "insert_test"
    assert result.json.get("data", {}).get("account_uid") == 1


def test_get_malls(app):
    with app.test_client() as client:
        result = client.get("/api/v1/malls")

    assert result.status_code == 200
    assert result.json
    assert result.json.get("count") == 0
    assert result.json.get("data") == []


def test_get_mall(app):
    with app.test_client() as client:
        client.post("/api/v1/accounts", json={"name": "account_test"})
        client.post("/api/v1/malls", json={"name": "insert_test", "account_uid": 1})
        result = client.get("/api/v1/malls/1")

    assert result.status_code == 200
    assert result.json
    assert result.json.get("data", {}).get("name") == "insert_test"


def test_put_mall(app):
    payload = {"name": "update_test", "account_uid": 1}

    with app.test_client() as client:
        client.post("/api/v1/accounts", json={"name": "account_test"})
        client.post("/api/v1/malls", json={"name": "insert_test", "account_uid": 1})
        result = client.put("/api/v1/malls/1", json=payload)

    assert result.status_code == 200
    assert result.json
    assert result.json.get("data", {}).get("name") == "update_test"


def test_delete_mall(app):
    with app.test_client() as client:
        client.post("/api/v1/accounts", json={"name": "account_test"})
        client.post("/api/v1/malls", json={"name": "insert_test", "account_uid": 1})
        result = client.delete("/api/v1/malls/1")

    assert result.status_code == 200
    assert result.json
    assert result.json.get("msg") == "Mall has been removed from DB"
