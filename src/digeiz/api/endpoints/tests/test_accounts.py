def test_post_account(app):
    payload = {"name": "insert_test"}

    with app.test_client() as client:
        result = client.post("/api/v1/accounts", json=payload)

    assert result.status_code == 201
    assert result.json
    assert result.json.get("data", {}).get("name") == "insert_test"


def test_get_accounts(app):
    with app.test_client() as client:
        result = client.get("/api/v1/accounts")

    assert result.status_code == 200
    assert result.json
    assert result.json.get("count") == 0
    assert result.json.get("data") == []


def test_get_account(app):
    with app.test_client() as client:
        client.post("/api/v1/accounts", json={"name": "insert_test"})
        result = client.get("/api/v1/accounts/1")

    assert result.status_code == 200
    assert result.json
    assert result.json.get("data", {}).get("name") == "insert_test"


def test_put_account(app):
    payload = {"name": "update_test"}

    with app.test_client() as client:
        client.post("/api/v1/accounts", json={"name": "insert_test"})
        result = client.put("/api/v1/accounts/1", json=payload)

    assert result.status_code == 200
    assert result.json
    assert result.json.get("data", {}).get("name") == "update_test"


def test_delete_account(app):
    with app.test_client() as client:
        client.post("/api/v1/accounts", json={"name": "insert_test"})
        result = client.delete("/api/v1/accounts/1")

    assert result.status_code == 200
    assert result.json
    assert result.json.get("msg") == "Account has been removed from DB"
