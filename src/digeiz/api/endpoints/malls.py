from flask import jsonify, make_response
from flask_restful import Resource, reqparse

from digeiz.api.models import Mall, Account
from digeiz.api.providers.db.schemas import mall_schema, malls_schema
from digeiz.api.utils.response import abort_service_error, abort_not_found, abort_invalid_request

form_parser = reqparse.RequestParser()
form_parser.add_argument("uid", type=int)
form_parser.add_argument("name", type=str, required=True, help="'name' has to be provided")
form_parser.add_argument(
    "account_uid",
    type=int,
    required=True,
    help="'account_uid' has to be provided to link new/given mall to a customer",
)

query_parser = reqparse.RequestParser()
query_parser.add_argument("next", type=int, default=1)


class MallListResource(Resource):
    def get(self):
        try:
            next_uid = query_parser.parse_args().get("next") if not 0 else 1
            if next_uid < 0:
                abort_invalid_request(msg="?next params can't be negative")

            all_malls = Account.query.all()
            malls = all_malls[next_uid - 1 :]

            if len(malls) > 2:
                next_uid = malls[next_uid - 1 : 3][-1].uid
                malls = malls[:2]
            else:
                next_uid = None

            return make_response(
                jsonify(
                    {
                        "data": malls_schema.dump(malls),
                        "count": len(malls),
                        "next": next_uid,
                    }
                ),
                200,
            )
        except Exception as e:
            return abort_service_error()

    def post(self):
        try:
            args = form_parser.parse_args()
            owner_account = Account.find_by_id(uid=args.get("account_uid"))

            if not owner_account:
                return abort_invalid_request(msg="There is no account for the given account_uid")

            if args.get("uid") and Mall.find_by_id(uid=args.get("uid")):
                return abort_invalid_request(msg="A mall already exist with the given uid")

            mall = Mall(
                uid=args.get("uid", None),
                name=args.get("name"),
                account_uid=args.get("account_uid"),
            )
            mall.add_to_db()
            return make_response(
                {
                    "msg": "Mall has been created and added in DB",
                    "data": {"uid": mall.uid, "name": mall.name, "account_uid": mall.account_uid},
                },
                201,
            )
        except Exception as e:
            return abort_service_error()


class MallResource(Resource):
    def get(self, mall_uid: int):
        try:
            account = Mall.query.get(mall_uid)
            if account is None:
                return abort_not_found(collection="mall", uid=mall_uid)
            else:
                return make_response(jsonify({"data": mall_schema.jsonify(account).json}), 200)
        except Exception as e:
            return abort_service_error()

    def delete(self, mall_uid: int):
        try:
            mall = Mall.find_by_id(uid=mall_uid)
            if not mall:
                return abort_not_found(collection="mall", uid=mall_uid)

            mall.delete_from_db()
            return make_response({"msg": "Mall has been removed from DB"}, 200)
        except Exception as e:
            return abort_service_error()

    def put(self, mall_uid: int):
        try:
            mall = Mall.find_by_id(uid=mall_uid)
            args = form_parser.parse_args()
            if not mall:
                return abort_invalid_request(msg="There is no mall for the given account_uid")

            if args.get("uid"):
                return abort_invalid_request(msg="you can't update a mall uid")

            mall.name = args.get("name", mall.name)
            mall.account_uid = args.get("account_uid", mall.account_uid)

            mall.update_in_db()
            return make_response(
                {
                    "msg": "Mall has been updated in DB",
                    "data": {"uid": mall.uid, "name": mall.name, "account_uid": mall.account_uid},
                },
                200,
            )
        except Exception as e:
            return abort_service_error()
