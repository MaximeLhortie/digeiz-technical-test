from setuptools import setup, find_packages

from src.digeiz.api import __VERSION__


with open("README.md", "r") as fh:
    long_description = fh.read()


EXTRAS_REQUIRE = {
    "docs": [],
    "tests": [
        "pytest==5.4.3",
        "coverage",
        "pytest-cov",
        "pytest-benchmark",
    ],
}
EXTRAS_REQUIRE["dev"] = EXTRAS_REQUIRE["docs"] + EXTRAS_REQUIRE["tests"]


setup(
    name="digeiz-api-test",
    version=__VERSION__,
    author="Maxime Lhortie",
    description="Tiny API for technical test",
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires=">=3.6",
    package_dir={"": "src"},
    packages=find_packages("src", exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    install_requires=[
        "Flask==1.1.4",
        "flask-marshmallow==0.13.0",
        "Flask-RESTful==0.3.8",
        "Flask-SQLAlchemy==2.4.2",
        "marshmallow==3.6.1",
        "marshmallow-sqlalchemy==0.23.1",
        "SQLAlchemy==1.3.17",
        "psycopg2-binary==2.8.5",
    ],
    # unit testing
    setup_requires=["pytest-runner"],
    tests_require=EXTRAS_REQUIRE["tests"],
    extras_require=EXTRAS_REQUIRE,
    # commands
    entry_points={"console_scripts": "digeiz-api-ws = digeiz.api.service:run"},
)
