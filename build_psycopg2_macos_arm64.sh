#!/bin/bash
#
# Script to generate arm64 wheels on an Apple Silicon host
#
# Based on https://github.com/psycopg/psycopg2/blob/cefb8181058342b9a4d0bd13b0fad920365d89d5/scripts/build/build_macos.sh

set -euo pipefail
set -x

prjdir=$(git rev-parse --show-toplevel)

brew install gnu-sed postgresql@9.6

# Find psycopg version
version=2.8.5
# A gratuitous comment to fix broken vim syntax file: '")
distdir="${prjdir}/dist/psycopg2-$version"
mkdir -p "$distdir"

# XXX: Hardcoding this value
export PACKAGE_NAME=psycopg2
# These values where extracted from https://www.djangocookbook.com/recipes/django-development-environment-on-apple-m1/
export PATH=/opt/homebrew/Cellar/postgresql@9.6/9.6.23/bin:$PATH
export LDFLAGS="-L/opt/homebrew/opt/openssl@1.1/lib"
export CPPFLAGS="-I/opt/homebrew/opt/openssl@1.1/include"

tempdir=$(mktemp -d /tmp/psycopg2.$$)
cd $tempdir
git clone https://github.com/psycopg/psycopg2.git
cd psycopg2
# XXX: Hard coding the version of psycopg2-binary to build
git checkout 2_8_5
venv="$(pwd)/venv"
# XXX: Hardcoding the Python version
/usr/local/bin/python3.8 -m virtualenv "${venv}"
source "${venv}/bin/activate"
distdir="${prjdir}/dist/psycopg2-$version"
mkdir -p "$distdir"

# Install required python packages
pip install -U pip wheel delocate

# Replace the package name
if [[ "${PACKAGE_NAME:-}" ]]; then
    gsed -i "s/^setup(name=\"psycopg2\"/setup(name=\"${PACKAGE_NAME}\"/" \
        "${prjdir}/setup.py"
fi

# Build the wheels
wheeldir="${prjdir}/wheels"
pip wheel --wheel-dir "${wheeldir}" --no-deps .
delocate-listdeps ${wheeldir}/*.whl
# This should be around 140KB
du -h ${wheeldir}/*.whl || exit 0

delocate-wheel --require-archs arm64 ${wheeldir}/*.whl
cp ${wheeldir}/*.whl ${distdir}
# This package should be close to 2MB rather than 140KB
du -h ${distdir}/*.whl

# Install and test the built wheel
brew services start postgresql@9.6

for i in $(seq 10 -1 0); do
    eval pg_isready && break
    if [ "$i" == 0 ]; then
        echo "PostgreSQL service not ready, giving up"
        exit 1
    fi
    echo "PostgreSQL service not ready, waiting a bit, attempts left: $i"
    sleep 5
done

pip install ${PACKAGE_NAME:-psycopg2} --no-index -f "$distdir"

# Print psycopg and libpq versions
python -c "import psycopg2; print(psycopg2.__version__)"
python -c "import psycopg2; print(psycopg2.__libpq_version__)"
python -c "import psycopg2; print(psycopg2.extensions.libpq_version())"
brew uninstall gnu-sed postgresql@9.6
set +x
deactivate
